<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1"/>
	<title><?php wp_title('-', true, 'right'); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="format-detection" content="telephone=no" />
	<meta name="author" content="">
	<?php wp_head(); ?>
</head>


<body>
<!--BEGIN HEADER-->

<div class="container">
	<header class="clearfix">
		<a href="<?php echo home_url(); ?>" class="logo"><h1>Allerion Solutions</h1></a>
		
			<?php if (!dynamic_sidebar('phone')) : ?>
			<?php endif; ?>
		
		<a href="javascript:void(0)" class="mobile-menu"></a>
		<nav>
			<?php add_filter('nav_menu_css_class', 'add_active_class_to_nav_menu'); ?>
			<?php wp_nav_menu(array('theme_location' => 'menu', 'menu_class' => 'clearfix', 'container'=>'false' )); ?>
			<?php remove_filter('nav_menu_css_class', 'add_active_class_to_nav_menu'); ?>
		</nav>
	</header>
</div>

<!--END HEADER-->