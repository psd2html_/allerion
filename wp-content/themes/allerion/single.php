<?php get_header(); ?>

<!--BEGIN CONTENT-->


<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>

        <div class="cover-one-article">

            <!-- TOP BACKGROUND IMAGE -->
            <?php dynamic_sidebar('single_image'); ?>

            <section class="container">
                <h2><?php the_title(); ?></h2>
                <div class="publication-cover clearfix">
                    <?php if ( get_post_type( get_the_ID() ) != 'type_projects' ) { ?>
                        <div class="publication">
                            <p class="blog-published">Published by <?php the_author_posts_link(); ?>
                                <span><?php the_time('F j, Y') ?></span>
                            </p>
                        </div>
                    <?php } ?>
                    <!-- SOCIALS -->
                    <?php include('includes/block-socials.php'); ?>

                </div>
                <div class="main">
                    <?php echo apply_filters('the_content', get_the_content()); ?>

                    <div class="bottom-content-cover clearfix">

                        <div class="tags">
                            <?php the_tags('<p><strong>Tags:</strong> ', ', ', '</p>'); ?>
                        </div>

                        <!-- SOCIALS -->
                        <?php include('includes/block-socials.php'); ?>
                    </div>
                </div>

                <?php if ( get_post_type( get_the_ID() ) != 'type_projects' ) { ?>
                    <?php related_posts(); ?>
                <?php } ?>

            </section>

        </div>
    <?php endwhile; ?>

<?php else : ?>

    <div class="post">
        <div class="head">
            <h1>Not Found</h1>
        </div>
        <div class="content">
            <p>Sorry, but you are looking for something that isn't here.</p>
        </div>
    </div>

<?php endif; ?>
<!--END CONTENT-->


<!--BEGIN SIGN UP-->
<?php include('includes/block-subscribe.php') ; ?>
<!--END SIGN UP-->

<!--END CONTENT-->

<?php get_footer(); ?>