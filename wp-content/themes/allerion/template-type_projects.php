<?php
/*
Template Name: Projects Template
*/
?>
<?php get_header(); ?>

<!--BEGIN CONTENT-->
<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
        <div class="project-top">
            <div class="container">
                <?php the_content(); ?>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>

<!--BEGIN PROJECTS-->
<?php
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    query_posts(array(
            'post_type' => 'type_projects',
            'post_status' => 'publish',
            'orderby' => 'date',
            'order' => 'DESC',
            'paged' => $paged
        )
    );
?>

<div class="project">
    <div class="container">


        <?php if ( have_posts() ) : ?>
            <?php while ( have_posts() ) : the_post(); ?>

                <div class="clearfix  project-box">


                    <div class="project-image">
                        <?php the_post_thumbnail('post-thumbnail'); ?>
                    </div>

                    <section class="project-info clearfix">
                        <?php the_title( sprintf( '<h2><a href="%s">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
                        
						<div class="project-info_box">
                            <?php the_excerpt(); ?>
                        </div>

                        <?php //the_tags('<p class="project-tags">Tags: ', ', ', '</p>'); ?>

                        <a href="<?php the_permalink() ?>" class="read-more">read more</a>
                    </section>
                </div>

            <?php endwhile; ?>

            <!-- PAGINATION -->
            <?php if (function_exists('wp_corenavi')) wp_corenavi(); ?>

        <?php endif; ?>
        <?php wp_reset_query(); ?>

    </div>
</div>

<!--END PROJECTS-->

<!--END CONTENT-->

<!--BEGIN SIGN UP-->
<?php include('includes/block-subscribe.php') ; ?>
<!--END SIGN UP-->

<!--END CONTENT-->

<?php get_footer(); ?>