<?php get_header(); ?>

    <!--BEGIN CONTENT-->
    <div class="blog-top">
        <div class="container clearfix">
            <div class="popular-tags">
                <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
                <?php /* If this is a category archive */ if (is_category()) { ?>
                    <h1>Archive for the &#8216;<?php single_cat_title(); ?>&#8217; Category</h1>
                    <?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
                    <h1>Posts Tagged &#8216;<?php single_tag_title(); ?>&#8217;</h1>
                    <?php /* If this is a daily archive */ } elseif (is_day()) { ?>
                    <h1>Archive for <?php the_time('F jS, Y'); ?></h1>
                    <?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
                    <h1>Archive for <?php the_time('F, Y'); ?></h1>
                    <?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
                    <h1>Archive for <?php the_time('Y'); ?></h1>
                    <?php /* If this is an author archive */ } elseif (is_author()) { ?>
                    <h1>Author Archive</h1>
                    <?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
                    <h1>Blog Archives</h1>
                <?php } ?>
            </div>
        </div>
    </div>

    <!--BEGIN BLOG-->
    <div class="blog">
        <div class="container">

            <?php if (have_posts()) : ?>
                <?php while (have_posts()) : the_post(); ?>

                    <div class="blog-box clearfix">
                        <div class="blog-image">
                            <?php the_post_thumbnail('post-thumbnail'); ?>
                        </div>
                        <section class="blog-info clearfix">
                            <?php the_title( sprintf( '<h2><a href="%s">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
                            <p class="project-published">Published by <?php the_author_posts_link(); ?><span><?php the_time('F j, Y') ?></span></p>
                            <div class="project-info_box">
                                <?php the_excerpt(); ?>
                            </div>
                            <?php the_tags('<p class="project-tags">Tags: ', ', ', '</p>'); ?>
                            <a href="<?php the_permalink() ?>" class="read-more">read more</a>
                        </section>
                    </div>

                <?php endwhile; ?>

                <!-- PAGINATION -->
                <?php if (function_exists('wp_corenavi')) wp_corenavi(); ?>


            <?php else : ?>

                <div class="post">
                    <div class="head">
                        <h1>Not Found</h1>
                    </div>
                    <div class="content">
                        <p>Sorry, but you are looking for something that isn't here.</p>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <!--END BLOG-->

    <!--END CONTENT-->

    <!--BEGIN SIGN UP-->
<?php include('includes/block-subscribe.php') ; ?>
    <!--END SIGN UP-->

    <!--END CONTENT-->

<?php get_footer(); ?>