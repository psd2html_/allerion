<section class="suggest">
    <h2>SUGGESTED ARTICLES</h2>
    <div class="suggest-cover">

        <?php if (have_posts()): ?>

            <?php while (have_posts()) : the_post(); ?>

                <div class="suggest-article">
                    <?php if (has_post_thumbnail()): ?>
                        <div class="suggest-image">
                            <?php the_post_thumbnail('medium'); ?>
                        </div>
                    <?php endif; ?>

                    <div class="suggest-info">
                        <h3>
                            <a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a>
                            <span> /<?php the_time('F j, Y'); ?></span>
                        </h3>
                        <?php remove_filter( 'the_excerpt', 'wpautop' ); ?>
                        <p><?php the_excerpt(); ?> <a href="<?php echo get_permalink(); ?>">Read More</a></p>
                        <?php add_filter( 'the_excerpt', 'wpautop' ); ?>
                    </div>
                </div>

            <?php endwhile; ?>

        <?php else: ?>

            <p>No related posts.</p>

        <?php endif; ?>
    </div>
</section>
