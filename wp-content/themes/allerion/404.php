<?php get_header(); ?>

    <div class="cover-about">
        <h2>Not Found</h2>
        <div class="description-about clearfix">


                <p>Sorry, but you are looking for something that isn't here.</p>

                <?php get_search_form(); ?>
        </div>
    </div>




<?php get_footer(); ?>