<?php get_header(); ?>

<?php  while ( have_posts() ) : the_post(); ?>
	<?php get_template_part( 'content', 'page' ); ?>
<?php endwhile; ?>

<!--BEGIN SIGN UP-->
<?php include(get_template_directory() . '/includes/block-subscribe.php') ; ?>
<!--END SIGN UP-->

<?php get_footer(); ?>