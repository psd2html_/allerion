<?php get_header(); ?>

<!--BEGIN CONTENT-->

<!--BEGIN POPULAR TAG OF BLOG-->
<?php
    $terms = get_terms(array(
            'taxonomy' => 'post_tag',
            'post_type' => array('post'),
            'hide_empty' => false)
    );
    $count_tags = count($terms);
?>
<?php if ($count_tags > 0) : ?>
    <div class="blog-top">
        <div class="container clearfix">
            <div class="popular-tags">
                <p><strong>Popular tags:</strong>
                    <?php
                        $x = 0;
                        foreach($terms as $t)
                        {
                            $x++;
                            echo '<a href="'. get_tag_link($t->term_id) .'">'. $t->name . '('. $t->count .')</a>';
                            if ($count_tags > $x) echo ', ';
                        }
                    ?>
                </p>

            </div>
            <div class="see-all">
                <span>see all</span>
            </div>
        </div>
    </div>
<?php endif; ?>
<!--END POPULAR TAG OF BLOG-->

<!--BEGIN BLOG-->

<div class="blog">
    <div class="container">

        <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>

                <div class="blog-box clearfix">
                    <div class="blog-image">
                        <?php the_post_thumbnail('post-thumbnail'); ?>
                    </div>
                    <section class="blog-info clearfix">
                        <?php the_title( sprintf( '<h2><a href="%s">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
                        <p class="project-published">Published by <?php the_author_posts_link(); ?><span><?php the_time('F j, Y') ?></span></p>
                        <div class="project-info_box">
                            <?php the_excerpt(); ?>
                        </div>
                        <?php the_tags('<p class="project-tags">Tags: ', ', ', '</p>'); ?>
                        <a href="<?php the_permalink() ?>" class="read-more">read more</a>
                    </section>
                </div>

            <?php endwhile; ?>

            <!-- PAGINATION -->
            <?php if (function_exists('wp_corenavi')) wp_corenavi(); ?>


        <?php else : ?>

            <div class="post">
                <div class="head">
                    <h1>Not Found</h1>
                </div>
                <div class="content">
                    <p>Sorry, but you are looking for something that isn't here.</p>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
<!--END BLOG-->

<!--END CONTENT-->

<!--BEGIN SIGN UP-->
<?php include('includes/block-subscribe.php') ; ?>
<!--END SIGN UP-->

<!--END CONTENT-->

<?php get_footer(); ?>