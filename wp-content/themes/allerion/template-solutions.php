<?php
/*
Template Name: Solutions Template
*/
?>
<?php
get_header(); ?>

<!--BEGIN CONTENT-->

<div class="solutions-general">
	<section class="container">
		<h2>solutions</h2>
		<?php if (!dynamic_sidebar('solutions')) : ?>
		<?php endif; ?>
	</section>
</div>

<!--END CONTENT-->

<!--BEGIN SIGN UP-->
<?php include(get_template_directory() . '/includes/block-subscribe.php') ; ?>
<!--END SIGN UP-->

<?php
get_footer();