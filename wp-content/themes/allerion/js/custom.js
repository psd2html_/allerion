/*
 * Custom
 */
$(document).ready(function() {

    $( ".sign-up-form form" ).attr( "novalidate", "novalidate" );

    $( ".sign-up-form form" ).on( "submit", function( event ) {

        var emailInput = $( ".sign-up-form input[type='email']" );
        var emailVal = emailInput.val();

/*        var textInput = $( ".sign-up-form input[type='text']" );
        var textVal = textInput.val();*/

        if (emailVal.trim() == '')
        {
            if ($('.field-email .alert').length === 0) {
                emailInput.after( '<span role="alert" class="alert wpcf7-not-valid-tip">The field is required.</span>' );
            }

            $('.sign-up-form .wpcf7-response-output').slideDown('fast');
            event.preventDefault();
        }

    });



});