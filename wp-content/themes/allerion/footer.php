<!--BEGIN FOOTER-->
<footer>
	<div class="footer-top">
		<div class="container clearfix">
			<nav>
				<?php add_filter('nav_menu_css_class', 'add_active_class_to_nav_menu'); ?>
				<?php wp_nav_menu(array('theme_location' => 'menu', 'menu_class' => 'clearfix', 'container'=>'false' )); ?>
				<?php remove_filter('nav_menu_css_class', 'add_active_class_to_nav_menu'); ?>
			</nav>
            <?php if (!dynamic_sidebar('social')) : ?>
            <?php endif; ?>
		</div>
	</div>
        <?php if (!dynamic_sidebar('copyright')) : ?>
        <?php endif; ?>
</footer>
<!--END FOOTER-->
<?php wp_footer(); ?>
</body>
</html>