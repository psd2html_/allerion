<div class="cover-about">
	<?php the_title( '<h2>', '</h2>' ); ?>
	<div class="description-about clearfix">

        <div class="logo-about clearfix">
            <div class="logo-about-image">
                <?php the_post_thumbnail(); ?>
            </div>
            <a href="<?php esc_url( get_permalink() ); ?>">Allerion Solutions</a>
        </div>

		<div class="entry-content">
			<?php
			/* translators: %s: Name of current post */
			the_content( sprintf(
				__( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'twentyfourteen' ),
				the_title( '<span class="screen-reader-text">', '</span>', false )
			) );

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentyfourteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
			) );
			?>
		</div><!-- .entry-content -->

	</div>
</div>

