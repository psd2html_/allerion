<?php
/*
Template Name: Home Template
*/
?>
<?php get_header(); ?>

    <!--BEGIN CONTENT-->

    <div class="cosmos">
        <img src="<?php bloginfo('template_url'); ?>/img/cosmos.jpg" alt="">
    </div>

    <!--BEGIN SOLUTIONS-->

    <div class="container">
        <section class="solutions">
            <h2>solutions</h2>
            <p>What kind of services do we offer?</p>
            <div class="cover clearfix">
                <?php if (!dynamic_sidebar('solutions-home')) : ?>
                <?php endif; ?>
            </div>
            <a href="/solutions" class="read-more">read more</a>
        </section>
    </div>

    <!--END SOLUTIONS-->

    <!--BEGIN ABOUT-->

    <div class="about">
        <section class="container">
            <h2>about</h2>
            <p>What kind of services do we offer?</p>
            <div class="cover clearfix">
                <?php if (!dynamic_sidebar('about-home-text')) : ?>
                <?php endif; ?>
            </div>
            <div class="cover clearfix">

                <?php if (!dynamic_sidebar('about-home-img')) : ?>
                <?php endif; ?>
            </div>
            <a href="/about" class="read-more">read more</a>
        </section>
    </div>

    <!--END ABOUT-->

    <!--BEGIN CONTACT-US-->
    <?php dynamic_sidebar('contact_form_home'); ?>
    <!--END CONTACT-US-->

    <!--END CONTENT-->


<!--BEGIN SIGN UP-->
<?php include('includes/block-subscribe.php') ; ?>
<!--END SIGN UP-->

<!--END CONTENT-->

<?php
get_footer();
