<?php

/**
 * Displaying the Full TinyMCE Editor
 */
function enable_more_buttons($buttons) {

    $buttons[] = 'fontselect';
    $buttons[] = 'fontsizeselect';
    $buttons[] = 'styleselect';
    $buttons[] = 'backcolor';
    $buttons[] = 'newdocument';
    $buttons[] = 'cut';
    $buttons[] = 'copy';
    $buttons[] = 'charmap';
    $buttons[] = 'hr';
    $buttons[] = 'visualaid';

    return $buttons;
}
add_filter("mce_buttons_3", "enable_more_buttons");

add_filter( 'tiny_mce_before_init', 'myformatTinyMCE' );
function myformatTinyMCE( $in ) {

    $in['wordpress_adv_hidden'] = FALSE;

    return $in;
}
/*************************/

function theme_name_scripts() {
    wp_enqueue_script('jquery.min', get_template_directory_uri() . '/js/jquery.min.js');
    wp_enqueue_script('js', get_template_directory_uri() . '/js/js.js');
    wp_enqueue_style('style', get_stylesheet_uri());
    wp_enqueue_style('editor_style', get_template_directory_uri() . '/css/editor-style.css');
    wp_enqueue_style('main_style', get_template_directory_uri() . '/main.css');
    wp_enqueue_style('custom', get_template_directory_uri() . '/css/custom.css');
}

add_action('wp_enqueue_scripts', 'theme_name_scripts');

add_action('wp_footer', 'add_scripts');
function add_scripts() {
    if(is_admin()) return false;
    wp_enqueue_script('custom', get_template_directory_uri().'/js/custom.js','','',true);
}

//Header Menu widgets
register_nav_menu('menu', 'Main menu');

add_filter('nav_menu_css_class', 'add_active_class_to_nav_menu');

function add_active_class_to_nav_menu($classes) {
    if (in_array('current-menu-item', $classes, true) || in_array('current_page_item', $classes, true)) {
        $classes = array_diff($classes, array('current-menu-item', 'current_page_item', 'active'));
        $classes[] = 'active';
    }
    return $classes;
}

$args = array(
    'name' => 'Phone',
    'id' => 'phone',
    'decription' => 'Add main site phone',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '',
    'after_title' => '',
);
register_sidebar($args);

//Fo0ter widgets
$args_copyright = array(
    'name' => 'Copyright',
    'id' => 'copyright',
    'decription' => 'Copyright blocks in footer',
    'before_widget' => '<div class="footer-bottom">',
    'after_widget' => '<div>',
    'before_title' => '',
    'after_title' => '',

);
register_sidebar($args_copyright);

$args_social = array(
    'name' => 'Social icons',
    'id' => 'social',
    'decription' => 'Social icons block',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '',
    'after_title' => '',
);
register_sidebar($args_social);


//Home Page widgets
$args_solutions_home = array(
    'name' => 'Solutions home page',
    'id' => 'solutions-home',
    'decription' => 'Solutions blocks on home page',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
    'before_widget' => '<section class="box">',
    'after_widget' => '</section>',
);
register_sidebar($args_solutions_home);

$args_solutions_about_text = array(
    'name' => 'About home page text',
    'id' => 'about-home-text',
    'decription' => 'About text blocks on home page',
    'class' => '',
    'before_widget' => '<div class="text-box">',
    'after_widget' => '</div>',
    'before_title' => '',
    'after_title' => '',
);
register_sidebar($args_solutions_about_text);

$args_solutions_about_img = array(
    'name' => 'About home page images',
    'id' => 'about-home-img',
    'decription' => 'About image blocks on home page',
    'class'         => 'nav-list',
    'before_widget' => '<figure  class="info-box">',
    'after_widget' => '</figure >',
    'before_title' => '<figcaption>',
    'after_title' => '</figcaption>',
);
register_sidebar($args_solutions_about_img);


//Solution Page
$args_solutions = array(
	'name' => 'Solutions page',
	'id' => 'solutions',
	'decription' => 'Solutions single page',
	'before_title' => '<div class="solutions-box_info"><h3>',
	'after_title' => '</h3>',
	'before_widget' => '<div class="solutions-box clearfix">',
	'after_widget' => '</div></div>',
);
register_sidebar($args_solutions);


$contact_form_home = array(
    'name' => 'Contact Form Home',
    'id' => 'contact_form_home',
    'decription' => 'Contact Form Home',
    'before_widget' => '<section class="contact-us container">',
    'after_widget' => '</section>',
    'before_title' => '<h2>',
    'after_title' => '</h2>',
);
register_sidebar($contact_form_home);


add_theme_support( 'post-thumbnails' );

function get_dynamic_sidebar($i = 1) {
    /* $c = '';
	 ob_start();
	 dynamic_sidebar($i);
	 $c = ob_get_clean();*/

   return $res = the_widget('WP_Widget_Text');



    /*
    $c = preg_replace('#<div class="box-image">(.*?)</div>#', '', $c);*/

    //$output = preg_replace("#<div class=\"box-image\">(.*?)</div>#is", "", $c);

    echo  $output;
}

/*function get_dynamic_sidebar($i = 1) {
    $c = '';
    ob_start();
    $c = ob_get_clean();

    echo "<pre>";
    print_r ($c);
    die();

    return $c;
    //return $c;
}
*/


/****************************************************/
// Thumbnails
if ( function_exists( 'add_theme_support' ) ) {
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size(420, 402, false); // Normal post thumbnails
    add_image_size( 'single-post-thumbnail', 400, 9999, true );
}


// Register post type
add_action( 'init', 'post_types_projects' );
function post_types_projects() {
	register_post_type( 'type_projects',
		array(
			'labels' => array(
				'name' => __( 'Projects' ),
				'singular_name' => __( 'Projects' ),
				'add_new_item' => __( 'Add New Project' ),
				'edit_item' => __( 'Edit Project' ),
			),
			'public' => true,
            'taxonomies' => array('post_tag'),
		)
	);
	add_post_type_support( 'type_projects', 'custom-fields' );
	add_post_type_support( 'type_projects', 'title');
	add_post_type_support( 'type_projects', 'editor');
	add_post_type_support( 'type_projects', 'thumbnail');
	add_post_type_support( 'type_projects', 'excerpt');
	add_post_type_support( 'type_projects', 'comments');
	register_taxonomy('provider-state', 'type_projects', array( 'hierarchical' => true, 'label' => 'State', 'query_var' => true, 'rewrite' => true ) );
}
add_theme_support('post-thumbnails', array('type_projects'));

// Handle the post_type parameter given in get_terms function
// see https://www.dfactory.eu/get_terms-post-type/
function df_terms_clauses($clauses, $taxonomy, $args) {
    if (!empty($args['post_type']))	{
        global $wpdb;

        $post_types = array();

        foreach($args['post_type'] as $cpt)	{
            $post_types[] = "'".$cpt."'";
        }

        if(!empty($post_types))	{
            $clauses['fields'] = 'DISTINCT '.str_replace('tt.*', 'tt.term_taxonomy_id, tt.term_id, tt.taxonomy, tt.description, tt.parent', $clauses['fields']).', COUNT(t.term_id) AS count';
            $clauses['join'] .= ' INNER JOIN '.$wpdb->term_relationships.' AS r ON r.term_taxonomy_id = tt.term_taxonomy_id INNER JOIN '.$wpdb->posts.' AS p ON p.ID = r.object_id';
            $clauses['where'] .= ' AND p.post_type IN ('.implode(',', $post_types).')';
            $clauses['orderby'] = 'GROUP BY t.term_id '.$clauses['orderby'];
        }
    }
    return $clauses;
}
add_filter('terms_clauses', 'df_terms_clauses', 10, 3);

// Pagination
function wp_corenavi()
{
    global $wp_query;
    $pages = '';
    $max = $wp_query->max_num_pages;
    if (!$current = get_query_var('paged')) $current = 1;
    $a['base'] = str_replace(999999999, '%#%', get_pagenum_link(999999999));
    $a['total'] = $max;
    $a['current'] = $current;

    $total = 0; //1 - выводить текст "Страница N из N", 0 - не выводить
    $a['mid_size'] = 5; //сколько ссылок показывать слева и справа от текущей
    $a['end_size'] = 1; //сколько ссылок показывать в начале и в конце
    $a['prev_text'] = 'prev'; //текст ссылки "Предыдущая страница"
    $a['next_text'] = 'next'; //текст ссылки "Следующая страница"

    if ($max > 1) echo '<div class="blog-dot clearfix"><ul class="clearfix">';
    if ($total == 1 && $max > 1) $pages = '<span class="pages">Страница ' . $current . ' из ' . $max . '</span>'."\r\n";
    //echo $pages . paginate_links($a);
    echo custom_paginate_links($a);
    if ($max > 1) echo '</ul></div>';

}
function custom_paginate_links( $args = '' ) {
    global $wp_query, $wp_rewrite;

    // Setting up default values based on the current URL.
    $pagenum_link = html_entity_decode( get_pagenum_link() );
    $url_parts    = explode( '?', $pagenum_link );

    // Get max pages and current page out of the current query, if available.
    $total   = isset( $wp_query->max_num_pages ) ? $wp_query->max_num_pages : 1;
    $current = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;

    // Append the format placeholder to the base URL.
    $pagenum_link = trailingslashit( $url_parts[0] ) . '%_%';

    // URL base depends on permalink settings.
    $format  = $wp_rewrite->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
    $format .= $wp_rewrite->using_permalinks() ? user_trailingslashit( $wp_rewrite->pagination_base . '/%#%', 'paged' ) : '?paged=%#%';

    $defaults = array(
        'base' => $pagenum_link, // http://example.com/all_posts.php%_% : %_% is replaced by format (below)
        'format' => $format, // ?page=%#% : %#% is replaced by the page number
        'total' => $total,
        'current' => $current,
        'show_all' => false,
        'prev_next' => true,
        'prev_text' => __('&laquo; Previous'),
        'next_text' => __('Next &raquo;'),
        'end_size' => 1,
        'mid_size' => 2,
        'type' => 'plain',
        'add_args' => array(), // array of query args to add
        'add_fragment' => '',
        'before_page_number' => '',
        'after_page_number' => ''
    );

    $args = wp_parse_args( $args, $defaults );

    if ( ! is_array( $args['add_args'] ) ) {
        $args['add_args'] = array();
    }

    // Merge additional query vars found in the original URL into 'add_args' array.
    if ( isset( $url_parts[1] ) ) {
        // Find the format argument.
        $format = explode( '?', str_replace( '%_%', $args['format'], $args['base'] ) );
        $format_query = isset( $format[1] ) ? $format[1] : '';
        wp_parse_str( $format_query, $format_args );

        // Find the query args of the requested URL.
        wp_parse_str( $url_parts[1], $url_query_args );

        // Remove the format argument from the array of query arguments, to avoid overwriting custom format.
        foreach ( $format_args as $format_arg => $format_arg_value ) {
            unset( $url_query_args[ $format_arg ] );
        }

        $args['add_args'] = array_merge( $args['add_args'], urlencode_deep( $url_query_args ) );
    }

    // Who knows what else people pass in $args
    $total = (int) $args['total'];
    if ( $total < 2 ) {
        return;
    }
    $current  = (int) $args['current'];
    $end_size = (int) $args['end_size']; // Out of bounds?  Make it the default.
    if ( $end_size < 1 ) {
        $end_size = 1;
    }
    $mid_size = (int) $args['mid_size'];
    if ( $mid_size < 0 ) {
        $mid_size = 2;
    }
    $add_args = $args['add_args'];
    $r = '';
    $page_links = array();
    $dots = false;

    if ( $args['prev_next'] && $current && 1 < $current ) :
        $link = str_replace( '%_%', 2 == $current ? '' : $args['format'], $args['base'] );
        $link = str_replace( '%#%', $current - 1, $link );
        if ( $add_args )
            $link = add_query_arg( $add_args, $link );
        $link .= $args['add_fragment'];

        /**
         * Filters the paginated links for the given archive pages.
         *
         * @since 3.0.0
         *
         * @param string $link The paginated link URL.
         */
        $page_links[] = '<li><a class="prev page-numbers" href="' . esc_url( apply_filters( 'paginate_links', $link ) ) . '">' . $args['prev_text'] . '</a></li>';
    endif;
    for ( $n = 1; $n <= $total; $n++ ) :
        if ( $n == $current ) :
            $page_links[] = "<li><a class='page-numbers current'>" . $args['before_page_number'] . number_format_i18n( $n ) . $args['after_page_number'] . "</a></li>";
            $dots = true;
        else :
            if ( $args['show_all'] || ( $n <= $end_size || ( $current && $n >= $current - $mid_size && $n <= $current + $mid_size ) || $n > $total - $end_size ) ) :
                $link = str_replace( '%_%', 1 == $n ? '' : $args['format'], $args['base'] );
                $link = str_replace( '%#%', $n, $link );
                if ( $add_args )
                    $link = add_query_arg( $add_args, $link );
                $link .= $args['add_fragment'];

                /** This filter is documented in wp-includes/general-template.php */
                $page_links[] = "<li><a class='page-numbers' href='" . esc_url( apply_filters( 'paginate_links', $link ) ) . "'>" . $args['before_page_number'] . number_format_i18n( $n ) . $args['after_page_number'] . "</a></li>";
                $dots = true;
            elseif ( $dots && ! $args['show_all'] ) :
                $page_links[] = '<span class="page-numbers dots">' . __( '&hellip;' ) . '</span>';
                $dots = false;
            endif;
        endif;
    endfor;
    if ( $args['prev_next'] && $current && ( $current < $total || -1 == $total ) ) :
        $link = str_replace( '%_%', $args['format'], $args['base'] );
        $link = str_replace( '%#%', $current + 1, $link );
        if ( $add_args )
            $link = add_query_arg( $add_args, $link );
        $link .= $args['add_fragment'];

        /** This filter is documented in wp-includes/general-template.php */
        $page_links[] = '<li><a class="next page-numbers" href="' . esc_url( apply_filters( 'paginate_links', $link ) ) . '">' . $args['next_text'] . '</a></li>';
    endif;
    switch ( $args['type'] ) {
        case 'array' :
            return $page_links;

        case 'list' :
            $r .= "<ul class='page-numbers'>\n\t<li>";
            $r .= join("</li>\n\t<li>", $page_links);
            $r .= "</li>\n</ul>\n";
            break;

        default :
            $r = join("\n", $page_links);
            break;
    }
    return $r;
}

add_filter( 'wpcf7_validate_configuration', '__return_false' );

// register tag [template_url]
function filter_template_url($text) {
    return str_replace('[template_url]',get_bloginfo('template_url'), $text);
}
add_filter('the_content', 'filter_template_url');
add_filter('get_the_content', 'filter_template_url');
add_filter('widget_text', 'filter_template_url');

// register tag [site-url]
function filter_site_url($text) {
    return str_replace('[site_url]',get_bloginfo('url'), $text);
}
add_filter('the_content', 'filter_site_url');
add_filter('get_the_content', 'filter_site_url');
add_filter('widget_text', 'filter_site_url');



$args_subscribers = array(
    'name' => 'subscribers',
    'id' => 'subscribers',
    'decription' => 'subscribers',
    'before_widget' => '<div class="sign-up"><section class="container">',
    'after_widget' => '</section></div>',
    'before_title' => '<h2>',
    'after_title' => '</h2>',
);
register_sidebar($args_subscribers);


$args_single_image = array(
    'name' => 'Single TopBg Image',
    'id' => 'single_image',
    'decription' => 'Single TopBg Image',
    'before_widget' => '<div class="bg-one-article">',
    'after_widget' => '</div>',
    'before_title' => '',
    'after_title' => '',
);
register_sidebar($args_single_image);