=== Image Text List Widget ===
Contributors: nickege
Tags: links, list, lists, widget

This plugin makes a widget available which allows you to add img and link list (bulleted or numbered) to a sidebar.
