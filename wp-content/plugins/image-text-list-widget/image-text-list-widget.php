<?php
/*
Plugin Name: Image Text List Widget
Description: Enables images, test and a link list widget, in which you can display items in an ordered or unordered list.
Author: nickege
Version: 1.0
*/

class ImageTextListWidget extends WP_Widget {
	
	public function __construct() {
		$widget_ops = array('classname' => 'widget_img_text_list', 'description' => __('Image Text List.'));
		parent::__construct(
			'imgTextList', // Base ID
			__('Image Text List'), // Name
			$widget_ops
		);
		
		add_action('admin_enqueue_scripts', array($this,'sllw_load_scripts'));
	}

	public function widget( $args, $instance ) {
		extract($args);
		$title = apply_filters('widget_title', empty($instance['title']) ? __('List') : $instance['title']);
		$body = apply_filters('widget_body', empty($instance['body']) ? '' : $instance['body']);
		$type = empty($instance['type']) ? 'unordered' : $instance['type'] ;
		$reverse = isset($instance['reverse']) ? $instance['reverse'] : false;
		$amount = empty($instance['amount']) ? 1 : $instance['amount'];

		$image_id     = $instance['image_id'];

		// Make sure the image ID is a valid attachment.
		if ( ! empty( $instance['image_id'] ) ) {
			$image = get_post( $instance['image_id'] );
			if ( ! $image || 'attachment' != get_post_type( $image ) ) {
				$output = '<!-- Image Widget Error: Invalid Attachment ID -->';
			}
		}
		
		for ($i = 1; $i <= $amount; $i++) {
			$items[$i-1] = $instance['item'.$i];
			$item_links[$i-1] = $instance['item_link'.$i];
			$item_classes[$i-1] = $instance['item_class'.$i];	
			$item_targets[$i-1] = isset($instance['item_target'.$i]) ? $instance['item_target'.$i] : false;
		}
		
		if($reverse){
			$items = array_reverse($items);
			$item_links = array_reverse($item_links);
			$item_classes = array_reverse($item_classes);
			$item_targets = array_reverse($item_targets);
		}

		/*echo '<pre>';
		print_r($image_id);
		die();*/


		echo $before_widget;

		if ( ! empty( $image_id ) ) {

			echo '<div class="box-image">';
			echo wp_get_attachment_image( $image_id , 'full' );
			echo '</div>';
		}

		echo $before_title . $title . $after_title;

		if ($body) {
			echo '<p>' . $body . '</p>';
		}


		if ($type == "ordered") { echo "<ol ";} else { echo("<ul "); } ?> class="list">

		<?php foreach ($items as $num => $item) : 
			if (!empty($item)) :
				echo("<li><p>" . $item . "</p></li>");
			endif;
		endforeach;
		
		echo $after_widget;
	}

	public function update( $new_instance, $old_instance) {
		//$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['image_id']   = absint( $new_instance['image_id'] );
		$instance['body'] = strip_tags($new_instance['body']);
		$amount = $new_instance['amount'];
		$new_item = empty($new_instance['new_item']) ? false : strip_tags($new_instance['new_item']);
		
		if ( isset($new_instance['position1'])) {
			for($i=1; $i<= $new_instance['amount']; $i++){
				if($new_instance['position'.$i] != -1){
					$position[$i] = $new_instance['position'.$i];
				}else{
					$amount--;
				}
			}
			if($position){
				asort($position);
				$order = array_keys($position);
				if(strip_tags($new_instance['new_item'])){
					$amount++;
					array_push($order, $amount);
				}
			}
			
		}else{
			$order = explode(',',$new_instance['order']);
			foreach($order as $key => $order_str){
				$num = strrpos($order_str,'-');
				if($num !== false){
					$order[$key] = substr($order_str,$num+1);
				}
			}
		}
		
		if($order){
			foreach ($order as $i => $item_num) {
				$instance['item'.($i+1)] = empty($new_instance['item'.$item_num]) ? '' : strip_tags($new_instance['item'.$item_num]);
				$instance['item_link'.($i+1)] = empty($new_instance['item_link'.$item_num]) ? '' : strip_tags($new_instance['item_link'.$item_num]);
				$instance['item_class'.($i+1)] = empty($new_instance['item_class'.$item_num]) ? '' : strip_tags($new_instance['item_class'.$item_num]);
				$instance['item_target'.($i+1)] = empty($new_instance['item_target'.$item_num]) ? '' : strip_tags($new_instance['item_target'.$item_num]);
			}
		}
		
		$instance['amount'] = $amount;
		$instance['type'] = strip_tags($new_instance['type']);
		$instance['reverse'] = empty($new_instance['reverse']) ? '' : strip_tags($new_instance['reverse']);

		return $instance;
	}

	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array(
			'title'      => '',
			'image_id'   => '',
			'body'       => '',
			'text'       => '',
			'title_link' => ''
		) );
		$title = strip_tags($instance['title']);
		$amount = empty($instance['amount']) ? 1 : $instance['amount'];

		$instance['image_id'] = absint( $instance['image_id'] );
		$image_id     = $instance['image_id'];

		$button_class = array( 'button', 'button-hero', 'simple-image-widget-control-choose' );
		
		for ($i = 1; $i <= $amount; $i++) {
			$items[$i] = empty($instance['item'.$i]) ? '' : $instance['item'.$i];
			$item_links[$i] = empty($instance['item_link'.$i]) ? '' : $instance['item_link'.$i];
			$item_classes[$i] = empty($instance['item_class'.$i]) ? '' : $instance['item_class'.$i];
			$item_targets[$i] = empty($instance['item_target'.$i]) ? '' : $instance['item_target'.$i];
		}
		$title_link = $instance['title_link'];		
		$type = empty($instance['type']) ? 'unordered' : $instance['type'] ;
		$reverse = empty($instance['reverse']) ? '' : $instance['reverse'];
?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>


		<p class="simple-image-widget-control<?php echo ( $image_id ) ? ' has-image' : ''; ?>"
		   data-title="<?php esc_attr_e( 'Choose an Image', 'simple-image-widget' ); ?>"
		   data-update-text="<?php esc_attr_e( 'Update Image', 'simple-image-widget' ); ?>"
		   data-target=".image-id">
			<?php
			if ( $image_id ) {
				echo wp_get_attachment_image( $image_id, 'medium', false );
				unset( $button_class[ array_search( 'button-hero', $button_class ) ] );
			}
			?>
			<input type="hidden" name="<?php echo esc_attr( $this->get_field_name( 'image_id' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'image_id' ) ); ?>" value="<?php echo absint( $image_id ); ?>" class="image-id simple-image-widget-control-target">
			<a href="#" class="<?php echo esc_attr( join( ' ', $button_class ) ); ?>"><?php _e( 'Choose an Image', 'simple-image-widget' ); ?></a>
		</p>


		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'body' ) ); ?>"><?php _e( 'Body:', 'simple-image-widget' ); ?></label>
			<textarea name="<?php echo esc_attr( $this->get_field_name( 'body' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'body' ) ); ?>" rows="4" class="widefat"><?php echo esc_textarea( $instance['body'] ); ?></textarea>
		</p>

		<div class="simple-link-list">
		<?php foreach ($items as $num => $item) :
			$item = esc_attr($item);
			$item_link = esc_attr($item_links[$num]);
		?>
		
			<div id="<?php echo $this->get_field_id($num); ?>" class="list-item">
				<div class="sllw-edit-item">
					<label for="<?php echo $this->get_field_id('item'.$num); ?>"><?php echo __("Text:"); ?></label>
					<input class="widefat" id="<?php echo $this->get_field_id('item'.$num); ?>" name="<?php echo $this->get_field_name('item'.$num); ?>" type="text" value="<?php echo $item; ?>" />
					<a class="sllw-delete hide-if-no-js"><img src="<?php echo plugins_url('images/delete.png', __FILE__ ); ?>" /> <?php echo __("Remove"); ?></a>
				</div>
			</div>
			
		<?php endforeach; 
		
		if ( isset($_GET['editwidget']) && $_GET['editwidget'] ) : ?>
			<table class='widefat'>
				<thead><tr><th><?php echo __("Item"); ?></th><th><?php echo __("Position/Action"); ?></th></tr></thead>
				<tbody>
					<?php foreach ($items as $num => $item) : ?>
					<tr>
						<td><?php echo esc_attr($item); ?></td>
						<td>
							<select id="<?php echo $this->get_field_id('position'.$num); ?>" name="<?php echo $this->get_field_name('position'.$num); ?>">
								<option><?php echo __('&mdash; Select &mdash;'); ?></option>
								<?php for($i=1; $i<=count($items); $i++) {
									if($i==$num){
										echo "<option value='$i' selected>$i</option>";
									}else{
										echo "<option value='$i'>$i</option>";
									}
								} ?>
								<option value="-1"><?php echo __("Delete"); ?></option>
							</select>
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			
			<div class="sllw-row">
				<input type="checkbox" name="<?php echo $this->get_field_name('new_item'); ?>" id="<?php echo $this->get_field_id('new_item'); ?>" /> <label for="<?php echo $this->get_field_id('new_item'); ?>"><?php echo __("Add New Item"); ?></label>
			</div>
		<?php endif; ?>
			
		</div>
		<div class="sllw-row hide-if-no-js">
			<a class="sllw-add button-secondary"><img src="<?php echo plugins_url('images/add.png', __FILE__ )?>" /> <?php echo __("Add Item"); ?></a>
		</div>

		<input type="hidden" id="<?php echo $this->get_field_id('amount'); ?>" class="amount" name="<?php echo $this->get_field_name('amount'); ?>" value="<?php echo $amount ?>" />
		<input type="hidden" id="<?php echo $this->get_field_id('order'); ?>" class="order" name="<?php echo $this->get_field_name('order'); ?>" value="<?php echo implode(',',range(1,$amount)); ?>" />

<?php
	}
	
	public function sllw_load_scripts($hook) {
		if( $hook != 'widgets.php') 
			return;
		if ( !isset($_GET['editwidget'])) {
			wp_enqueue_script( 'sllw-sort-js', plugin_dir_url(__FILE__) .'js/sllw-sort.js');
			wp_enqueue_script( 'simple-image-widget-js', plugin_dir_url(__FILE__) .'js/simple-image-widget.js');
		}
		wp_enqueue_style( 'sllw-css', plugin_dir_url(__FILE__) .'css/sllw.css');
	}

}

add_action('widgets_init', create_function('', 'return register_widget("ImageTextListWidget");'));
?>