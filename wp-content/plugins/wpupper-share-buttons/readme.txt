=== WPUpper Share Buttons ===

Author URL: https://github.com/victorfreitas
Contributors: victorfreitas
Donate link: https://www.redcross.org/donate/donation
Tags: share, social, buttons, share buttons, compartir, botones compartir, free share buttons, whatsapp, facebook, twitter, google plus, compartilhar, redes sociais, social plugin, tweet button, share image, sharebar, sharing, social bookmarking, email form, social media buttons, click to tweet, reddit, viber, telegram, gmail, skype, like, pinterest, linkedin
Requires at least: 3.0.0
Tested up to: 4.6.1
Stable tag: 3.15
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Implement the Share Buttons of the major social networks, including the Whats App on your website or blog.

== Description ==

> Required PHP version 5.2.4 or above

Implement the Share Buttons of the major social networks, including the Whats App on your website or blog. The buttons are inserted automatically or can be called via shortcode or PHP method.

> If you like the plugin, feel free to rate it (on the right side of this page) or [donate](https://www.redcross.org/donate/donation). I am very pleased to dedicate myself to this plugin. Thank you so much! :)

* Adds before, after the contents post or both

* Create a new table to store the sharing number of posts

* Store some options with the data that will be used to show, hide, stylize and remove specific buttons.

* The networks support are: - Facebook; - Google Plus; - Twitter; - Linkedin; - Pinterest; - Thumbler; - Whatsapp; - Mailto; - Print Friendly; Telegram; Skype; Viper; - Reddit; - Gmail

== Installation ==

* Upload the files to the `/wp-content/plugins/` (keep the original folder plugin)

* Ative o plugin na interface de 'Plugins' do WordPress

* Click Settings to go to the page of the plugin or the Social Buttons menu

== Frequently Asked Questions ==

* Some social networks already have shares counters. Facebook, Linkedin, Pinterest and Google Plus

== Thanks ==

Translators who did a great job converting the text of the plugin to their native language. Thank you!

* [Victor Freitas](https://github.com/victorfreitas/) (Brazilian)
* [Victor Freitas](https://github.com/victorfreitas/) (Spanish)
* [Lily Ousborne](http://burzhu.net/) (Russian)

* Note: This is very important for all users worldwide.
 So please contribute your language to the plugin to make it even more useful.
 Translating validating in ["Poedit Editor"](http://www.poedit.net/).

== Screenshots ==

1. Admin - Options Page. Possibility of ordering social networks
2. Admin - Options Page. Preview buttons
3. Admin - Extra settings page
4. Admin - Use options page
5. Admin - Sharing report page
6. After button click share
7. Layout: Square Plus with counter
8. Layout: Square Plus - No counter
9. Layout: Square Plus - Mobile landscape
10. Layout: Square Plus - Mobile portrait
11. Layout: Default
12. Layout: Default - No counter
13. Layout: Default - No title and counter
14. Layout: Buttons
15. Layout: Buttons - No counter
16. Layout: Buttons - No tile and counter
17. Layout: Rounded
18. Layout: Rounded - No counter
19. Layout: Rounded - No title and counter
20. Layout: Rounded - Mobile portrait
21. Layout: Square
22. Layout: Square - No counter
23. Layout: Square - No title and counter
24. Layout: Square - Mobile portrait
25. Layout: Buttons - Fixed left with counter
26. Layout: Buttons - Fixed left toggle
27. Layout: Buttons - Fixed left no counter
28. Layout: Buttons - Fixed left mobile portrait
29. Layout: Buttons - Fixed left mobile landscape
30. Layout: Buttons - Fixed left tablet portrait

* Share buttons
* Settings panel
* Set for your sharing icons

== Recommended Plugins ==

The following plugins are recommended

* ["Remove WP Version"](https://wordpress.org/plugins/jogar-mais-wp-security/) by victorfreitas - Remove version WordPress in WP Generator, Feeds, css and js file.

== Changelog ==

= 3.15 =

* Required PHP version 5.2.4 or above
* Bug fix permalink params

= 3.14 =

* Bug fix PHP version < 5.3.0 on plugin activation
* Displaying formatted share count
* Change text color total counts for layout square plus

= 3.13 =

* New: Share count support on Tumblr
* Update Tumblr share API url
* Update filters support
* Bug fix

= 3.12 =

* New button: Facebook Messenger
* Add message on plugin admin page
* Update languages
* Bug fix

= 3.11 =

* Performance improvements
* Bug fix
* Change class name WPUSB_All_Items to WPUSB_Modal

= 3.10 =

* Bug fix GLOB_BRACE flag is not available on some non GNU systems

= 3.9.0 =

* General coding standards and improvements
* Improvements modal social networks
* New button: Share on Flipboard
* New: Option set url via shortcode or method php
* New: Option set title via shortcode or method php
* Bug fix

= 3.8.2 =

* Add filter classes icons

= 3.8.1 =

* Add filter prefix classes icons
* Bug fix

= 3.8.0 =

* Improvements performance
* Adding option on extra settings for to keep CSS in the footer, this is good for performance of your website.
* Update translations

= 3.7.0 =

* New: Option to choose which social networks show through shortcode, PHP method
* You can see more details on using the shortcode and PHP method on the "Use Options" in the administration of the plugin.

= 3.6.6 =

* Bug fixed: Shares count because of the update of the Facebook API response

= 3.6.5 =

* Compatible up to: 4.6

= 3.6.4 =

* Bug fix (https://wordpress.org/support/topic/not-sharing-the-article-sharing-the-web-site-address-instead?replies=1)

= 3.6.3 =

* Bug fix

= 3.6.2 =

* Bug fix multisite admin page

= 3.6.1 =

* Bug fix
* Removed persistent message

= 3.6.0 =

* Bug fixed
* Fixed fatal error on first install
* Fixed security requests ajax
* Update translations
* Remove fields: "Remove count" and "Remove title" from extra settings
* Add fields: "Remove count" and "Remove title" in General Settings

= 3.5.4 =

* Bug fix css layout fixed

= 3.5.3 =

* Bug fix

= 3.5.2 =

* Bug fix

= 3.5.1 =

* Bug fix

= 3.5.0 =

* New: Layout rounded for position fixed
* Internal improvements
* Bug fix
* Update translations

= 3.4.2 =

* Bug fix

= 3.4.1 =

* General internal improvements
* Performance improvements
* Bug fix

= 3.4.0 =

* New: Add share on Reddit
* Updating translations

= 3.3.0 =

* Bug fix
* New: Possiblity add buttons share on Archive and Category
* Minor improvements

= 3.2.1 =

* Bug fix

= 3.2.0 =

* New: Layout fixed by context
* Internal Improvements
* Updating translations
* Bug fix

= 3.1.3 =

* Internal Improvements
* Improvements in the colors of the buttons on administration page
* Improved checkbox buttons in administration page
* Updating translations
* Bug fix

= 3.1.2 =

* Add custom PHP method to include the buttons.

= 3.1.1 =

* Bug fixed in check referrer

= 3.1.0 =

* New position fixed right
* New send by Gmail
* Improvements performance
* Update translations
* Bug fix

= 3.0.2 =

* Bug fix in links

= 3.0.1 =

* Bug fix PHP < 5.5

= 3.0.0 =

* Enabling modal to show all buttons.
* Adding one more button to open modal social networks.
* Exchanging email color and icon.
* Improvements buttons.
* Enabling the possibility of having fixed buttons on the left and content.
* Improving animation on hover of fixed buttons left.
* Enabling highlight of buttons by reference.
* internal improvements.
* Updating translations.
* Update fonts.
* Bug fix.

= 2.9.3 =

* Bug fixed checked persistent items in admin settings

= 2.9.2 =

* Bug fix

= 2.9.1 =

* Bug fix

= 2.9.0 =

* Bug fix
* Add position fixed top buttons
* Add option Twitter text in extra settings page
* Add top menu in the settings
* Change color inputs in the settings page
* Update translations
* Tested in version 4.5.1

= 2.8.2 =

* Bug fix

= 2.8.1 =

* Updated Russian translation

= 2.8.0 =

* New button - like on facebook

= 2.7.0 =

* Improvements layout fixed left
* Responsive layout fixed left
* New screenshots

= 2.6.1 =

* Bug fix

= 2.6.0 =

* Tested in version 4.5.0
* Add icons sharing for Telegram, Skype and Viber
* Bug fix
* Improvements performance
* Update langs pt_BR and es_ES

= 2.5.3 =

* Fixed bug links social networks share unsafe script when https

= 2.5.2 =

* Fixed bug

= 2.5.1 =

* Update translations

= 2.5.0 =

* Bug fix
* Improvements
* new icons for filter

= 2.4.1 =

* Bug fix

= 2.3.1 =

* Improvements internal

= 2.3.0 =

* Bug fixed in buttons text
* Bug fixed in sortable share report page
* Improvements
* Updating the translations
* Implements search in share report page

= 2.2.4 =

* Bug fix UTM Tracking option
* Change slug filters and page
* Improvements button style printfriendly
* Effect hide buttons fixed left

= 2.2.3 =

* Bug fix counter sharing, before and after buttons on content. [Related by athalas]

= 2.2.2 =

* Bug fix whatsapp UTM Tracking params

= 2.2.1 =

* Bug fix
* Update translations, Improvements short url, change namespace js

= 2.2.0 =

* Bug fix
* adds shortener url with bitly

= 2.1.1 =

* Bug fix whatsapp icon
* Bug fix counter markup style in theme Square Plus
* Improvements perfomance preview share buttons in settings page
* Add spinner before preview share buttons

= 2.1.0 =

* Bug fix and improvements performance
* Adding possibility to order the buttons of social networks
* Style of buttons
* Update Google Plus icon
* Preview share buttons in settings page

= 2.0.4 =

* Bug fix pagination sharing report page [Related by nagal #post-7832935]

= 2.0.3 =

* Bug fix icon email, whatsapp. [Related by theadarshmehta]
* Bug fix translate title icon email, printfriendly. [Related by theadarshmehta]
* Bug fix sharing report admin page in WordPress 4.4
* Tested in WordPress 4.4
* Releasing some filters for customization dev
* Logo
* Performance improvements

= 2.0.2 =

* Remove twitter counter, API discontinued

= 2.0.1 =

* Bug fix

= 2.0.0 =

* new layouts
* Improvements in settings
* new features
* new customizations
* Removing old layouts that were broken
* Performance improvements
* Bug fix

= 1.4.2 =

* Bug fix
* Improving performance

= 1.4.1 =

* Bug fix

= 1.4.0 =

* Preparing for internationalization
* Adding English translation
* Adding Spanish translation
* Change button style theme 3

= 1.3.0 =

* Adding WP List Table in the sharing report page
* Small internal improvements

= 1.2.1 =

* Tested WordPress version 4.3
* Validations in the use of PHP method to call buttons
* Option remove scripts in front
* Option to time set cache reports of the page sharing of posts
* Bug fix

= 1.2.0 =

* Improving performance in SQL queries

= 1.1.1 =

* Bug fix

= 1.1.0 =

* Theme
* Optimize icons mobile
* Icons style
* Bug fix

= 1.0.5 =

* Bug fix

= 1.0.3 =

* Adds submenu page
* Page report share counter
* Counter for google plus
* Submenus
* Code patterns fix

= 1.0.2 =

* Adds option UTM tracking for analytics
* Patterners fix style admin page
* Change screenshots
* Code patterns fix

= 1.0.1 =

* Layout admin page configurations
* Change layout buttons secondary
* Fix style layout buttons primary
* Code patterns fix

= 1.0.0 =

* Initial release

== Upgrade Notice ==

= 3.15 =

* Required php version 5.2.4 or above