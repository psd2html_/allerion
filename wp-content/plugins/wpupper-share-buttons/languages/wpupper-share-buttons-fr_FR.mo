��    H      \  a   �            !  $   >     c     q     �  
   �     �  �   �     N     V     b  &   q     �     �  e   �  
             )     .     ?     H     X     f     m     r     �  	   �     �     �     �     �     �     �     �     �     	     	  F   ,	     s	     |	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     .
     =
     O
  &   ^
     �
     �
  +   �
     �
     �
     �
     �
     �
     �
     �
       #     !   >     `     n     �     �  �  �  $   5  -   Z     �     �     �     �     �  �   �  
   �      �     �     �     �       j   	     t     �     �     �     �     �     �     �     �      �               &     /     J     R     l     x     �     �     �     �  V   �     8     P     j     s     �     �     �     �     �     �     	          3     F     \     o     �     �  2   �     �     �     �     �     �     �     �          2  /   G     w     �     �     �     B       "      %   H           .         6   5      $   2                     =      9      )   7   8   (             +               A          #   0              F              '       !       &         -      @   /      *   ;   >   G                                        ?      <   
   3         4       :      C      E          ,   D   	   1          Add UTM tracking (Analytics) Add the Share Buttons automatically. After content Before content Button Cache time Click to see also Could not complete installation of <strong>WPUpper Share Buttons</strong> because your PHP version is less than 5.3.0, please upgrade your PHP version to use the plugin. Default Disable CSS Extra Settings Extra Settings | WPUpper Share Buttons Facebook Google+ Help keep the free %s, you can make a donation or vote with %s at WordPress.org. Thank you very much! I just saw Layout options Like Like on Facebook Linkedin Make a donation Make a review Minute More Open modal social networks Pages Pinterest Print Print via PrintFriendly Rounded Save Changes Search See with count See with title Send by Gmail Send by email Send on Facebook Messenger Set the time in minutes that will be cached in the Sharing report page Settings Settings saved. Share Share on Facebook Share on Flipboard Share on Google+ Share on Linkedin Share on Pinterest Share on Reddit Share on Skype Share on Telegram Share on Tumblr Share on Viber Share on WhatsApp Sharing Report Sharing Report | WPUpper Share Buttons Single Square There is no record available at the moment! Title Total Tweet Twitter Twitter text Twitter username Use <code>?</code> and Use options Use options | WPUpper Share Buttons Use {title} to add the post title View Untitled View without count Your twitter username minutes Project-Id-Version: Development (trunk)
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/wpupper-share-buttons
POT-Creation-Date: 2016-11-07 21:53-0200
PO-Revision-Date: 2016-11-07 21:54-0200
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Poedit 1.8.8
 Ajouter un UTM  tracking (Analytics) Ajouter les boutons Partager automatiquement. Après le contenu Avant le contenu Bouton Temps de cache Cliquez pour voir aussi Impossible de terminer l'installation de <strong>WPUpper Share Buttons</strong> car votre version PHP est inférieure à 5.3.0, veuillez mettre à niveau votre version PHP pour utiliser le plugin. Pa défaut Désactiver la feuille de style. Paramètres extra Paramètres supplémentaires Facebook Google Aidez à garder les %s libres, vous pouvez faire un don ou voter avec %s à WordPress.org. Merci beaucoup! Je viens de voir Options de présentation  Comme Comme sur Facebook Linkedin Faire un don Faire un commentaire Minutes Plus réseaux sociaux modales ouverts Page Pinterest&nbsp; Imprimer Imprimer via PrintFriendly Arrondi Sauvegarder les réglages Rerchercher Voir avec le comte Voir avec le titre Envoyé par Gmail Envoyé par email Envoyé par Facebook Messenger Réglez le temps en minutes qui seront mis en cache dans la page du rapport de partage Paramètres (Réglages) Paramètres enregistrés. Partagez Partager sur Facebook Partager sur Flipboard Partager sur Google+ Partager sur Linkedin Partager sur Pinterest Partager sur Reddit Partager sur Skype Partager sur Telegram Partager sur Tumblr Partager sur Viber Partager sur WhatsApp Rapport de partage Rapport de partage Simple Carrée Il n'y a pas de dossier disponible pour le moment! Titre : Total Tweeter Twitter Texte Twitter Compte Twitter Utilisation <code>?</code> et Utilisez les options Utilisez les options Utilisez {title} pour ajouter le titre de poste Voir Untitled Voir sans compte Votre nom d'utilisateur twitter  minutes 